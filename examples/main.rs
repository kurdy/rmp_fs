/*
    Copyright 2020

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
    documentation files (the "Software"), to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions
    of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
    TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
    THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#[macro_use]
extern crate serde_derive;

use rmp_fs::{save_to_rmp, load_from_rmp};
use std::path::Path;

// Define a struct of data
#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct ExampleData {
    value: i64,
    message: String,
}

fn main() {
    let file_name = Path::new("test_rmp_fs.blob");

    let data = ExampleData {
        value: -123456,
        message: "Example rmp_fs".to_string()
    };

    // Save to a file it return Result<(), Box<dyn Error>>
    save_to_rmp(file_name, &data).expect("Fail to save as rmp.");

    // Load from a file it return Result<T, Box<dyn Error>>
    let data2: ExampleData =load_from_rmp(file_name).expect("Fail to load from rmp.");
    println!("{:?}\n{:?}",data,data2);
}