![Licence](https://img.shields.io/crates/l/rmp_fs) ![Version](https://img.shields.io/crates/v/rmp_fs) [![dependency status](https://deps.rs/repo/gitlab/kurdy/rmp_fs/status.svg)](https://deps.rs/repo/gitlab/kurdy/rmp_fs) ![Download](https://img.shields.io/crates/d/rmp_fs)
# README #

# rmp_fs
A small lib that help to serialize and deserialize from/to rmp (RustMessagePack) with a file. It does nothing special, just encapsulates rmp_serde into functions that can be used repeatedly.

## Example:

```rust
#[macro_use]
extern crate serde_derive;

use rmp_fs::{save_to_rmp, load_from_rmp};
use std::path::Path;

// Define a struct of data
#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct ExampleData {
    value: i64,
    message: String,
}

fn main() {
    let file_name = Path::new("test_rmp_fs.blob");

    let data = ExampleData {
        value: -123456,
        message: "Example rmp_fs".to_string()
    };

    // Save to a file it return Result<(), Box<dyn Error>>
    save_to_rmp(file_name, &data).expect("Fail to save as rmp.");

    // Load from a file it return Result<T, Box<dyn Error>>
    let data2: ExampleData =load_from_rmp(file_name).expect("Fail to load from rmp.");
    println!("{:?}\n{:?}",data,data2);
}
```

### Note: 
According my test the struct below has a size of 248 bytes using rmp and 389 bytes using cbor

```rust
#[derive(Debug, PartialEq, Deserialize, Serialize)]
    struct StructTest01 {
        text : String,
        value1 : u16,
        value2 : i32,
        value3 : u8,
        value4 : i16,
        value5 : u32,
        value6 : u64,
        value7 : i64,
        value8 : usize,
        value9 : f32,
        value10 : f64,
        array : Vec<u8>,
        tuple : (DateTime<Utc>,u64),
        complex : StructTest02,
    }

    #[derive(Debug, PartialEq, Deserialize, Serialize)]
    struct StructTest02 {
        data_v : Vec<i16>,
        data_m : HashMap<String,String>,
        state : bool,
    }
```
